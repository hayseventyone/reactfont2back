import React from "react";

export default props => {
  return (
    <div>
      <h1 className="display-4">About Manage Contact</h1>
      {/* {props.match.params.id} */}
      <p className="lead">Simple app to manage contact</p>
      <p className="text-secondary">Version 1.0.0</p>
    </div>
  );
};
