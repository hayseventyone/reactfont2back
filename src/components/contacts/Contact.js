//
import React, { Component } from "react";
import { Link } from "react-router-dom";
import propsType from "prop-types";
import { Consumer } from "../../context";
import axios from "axios";

class Contact extends Component {
  state = {
    isShowContact: false
  };
  onShowClick = () => {
    this.setState({
      isShowContact: !this.state.isShowContact
    });
  };
  onDeleteClick = async (id, dispatch) => {
    try {
      await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`);
      dispatch({
        type: "DELETE_CONTACT",
        payload: id
      });
    } catch (e) {
      //alway del from context
      dispatch({
        type: "DELETE_CONTACT",
        payload: id
      });
    }
  };
  render() {
    const { name, email, phone, id } = this.props.contact;
    const { isShowContact } = this.state;
    return (
      <Consumer>
        {value => {
          const { dispatch } = value;
          return (
            <div className="card card-body mb-3">
              <h4>
                {name}{" "}
                <i onClick={this.onShowClick} className="fas fa-sort-down" />
                <i
                  className="fas fa-times"
                  style={{ cursor: "pointer", float: "right", color: "red" }}
                  onClick={this.onDeleteClick.bind(this, id, dispatch)}
                />
                <Link to={`contact/edit/${id}`}>
                  <i
                    className="fas fa-pencil-alt"
                    style={{
                      cursor: "pointer",
                      float: "right",
                      color: "black",
                      marginRight: "1em"
                    }}
                  />
                </Link>
              </h4>
              {isShowContact ? (
                <ul className="list-group">
                  <li className="list-group-item"> email:{email}</li>
                  <li className="list-group-item">Phone:{phone}</li>
                </ul>
              ) : null}
            </div>
          );
        }}
      </Consumer>
    );
  }
}

Contact.propsType = {
  contact: propsType.object.isRequired
};
export default Contact;
