import React, { Component } from "react";
import uuid from "uuid";
import { Consumer } from "../../context";
import TextInputGroup from "../layout/TextInputGroup";
import axios from "axios";

class NewContact extends Component {
  state = {
    name: "",
    email: "",
    phone: "",
    errors: {}
  };

  SetError = errors => {
    this.setState({ errors });
  };
  onSubmit = async (dispatch, e) => {
    e.preventDefault();
    const { name, email, phone } = this.state;

    // check required Field
    if (name === "") {
      this.SetError({ name: "Name is required" });
      return;
    }
    if (email === "") {
      this.SetError({ email: "email is required" });
      return;
    }
    if (phone === "") {
      this.SetError({ phone: "Phone is required" });
      return;
    }
    const newContact = {
      id: uuid(),
      name,
      email,
      phone
    };

    const res = await axios.post(
      "https://jsonplaceholder.typicode.com/users",
      newContact
    );
    dispatch({
      type: "ADD_CONTACT",
      payload: res.data
    });

    this.clearState();

    this.props.history.push("/");
  };
  clearState = () => {
    this.setState({
      name: "",
      email: "",
      phone: "",
      errors: {}
    });
  };

  onChangeValue = e => this.setState({ [e.target.name]: e.target.value });
  render() {
    const { name, email, phone, errors } = this.state;
    return (
      <Consumer>
        {value => {
          const { dispatch } = value;
          return (
            <div className="card mb-3">
              <div className="card-header">New Contact na</div>
              <div className="card-body">
                <form onSubmit={this.onSubmit.bind(this, dispatch)}>
                  <TextInputGroup
                    label="Name"
                    type="text"
                    name="name"
                    placeholder="Enter Name.."
                    value={name}
                    onChange={this.onChangeValue}
                    error={errors.name}
                  />

                  <TextInputGroup
                    label="Email"
                    type="email"
                    name="email"
                    placeholder="Email@Email.com "
                    value={email}
                    onChange={this.onChangeValue}
                    error={errors.email}
                  />

                  <TextInputGroup
                    label="Phone"
                    type="text"
                    name="phone"
                    placeholder="Enter Phone"
                    value={phone}
                    onChange={this.onChangeValue}
                    error={errors.phone}
                  />

                  <input
                    type="submit"
                    value="New Contact"
                    className="btn btn-light btn-block"
                    //onChange={this.onChangeValue}
                  />
                </form>
              </div>
            </div>
          );
        }}
      </Consumer>
    );
  }
}

export default NewContact;
